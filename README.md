### Prerequisite

- Node.js visit on : https://nodejs.org/en/download/
- Angular visit on : https://angular.io/guide/quickstart
- Visual Studio Code visit on : https://code.visualstudio.com/download
- Version Control Subversion or Git-Bash (github desktop)
- Maven
- IDE : Eclipse, Intelij, or other
- Java 8 is a must 😀


### Installation Angular

- Install Node JS & Check version
- Install Angular CLI & Check version

### Live Demo